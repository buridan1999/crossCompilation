@echo off
@:Setlocal EnableDelayedExpansion

set PROGRAMM_NAME=app
set PROGRAMM_EXTENSION=exe
set TARGETS=i386-pc-windows x86_64-pc-windows

Setlocal EnableDelayedExpansion
for %%T in (%TARGETS%) do (
	echo Compilation for %%T
	set PROGRAMM_NAME_ARCH=%PROGRAMM_NAME%_%%T.%PROGRAMM_EXTENSION%
	set COMPILATION_LOG="CompilationLog_%%T.txt"
	
	echo Programm: !PROGRAMM_NAME_ARCH!
	echo Log file: !COMPILATION_LOG!
	clang-cl main.cpp --target=%%T -o !PROGRAMM_NAME_ARCH! > !COMPILATION_LOG!
	
	echo %ERRORLEVEL%
	if %ERRORLEVEL% EQU 0 (
	  echo "Compiled!"
	  
	  
	)
	if %ERRORLEVEL% NEQ 0 (
	  echo "Error!"
	  
	  
	)

	echo "End!"
	
	echo.
	echo.	
)
Endlocal

echo "All compiled!"
pause